function addTokens(input, tokens){
    if(typeof input != "string")
        throw "Invalid input"
    if(input.length  < 6)
        throw "Input should have at least 6 characters"
    tokens.every((x) => {
        if(typeof x.tokenName != "string")
            throw "Invalid array format"
    })

    var strings = input.split("...")
    if(strings.length > 1)
        tokens.every((x, i) => {
            strings[i] = strings[i].concat("${" + x.tokenName + "}")
        })
    
    return strings.join("")
}

const app = {
    addTokens: addTokens
}

app.addTokens('Subsemnatul ...', [{tokenName: 'subsemnatul'}])

module.exports = app;